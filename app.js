const express = require('express');
const app = express();
const MongoClient = require('mongodb').MongoClient
const port = 5000;
const bodyParser = require('body-parser');
const logger = require('./logger')

app.use(bodyParser.json())


const client = new MongoClient("mongodb://localhost:27017/latestdb",{
    useNewUrlParser:true,
    useUnifiedTopology: true
})


    const myDB = client.db('latestdb').collection('users')
    
    app.get('/users',(req,res)=>{
        myDB.find().toArray()
        .then(results=>{
            if(results.length==0)
            {
                logger.error("No users found")
                res.send("Error")
            }
            else{
                logger.info(results)
                console.log(results)
                res.contentType('application/json')
                res.send(JSON.stringify(results))
            }          
        })
    })

    app.post('/addUser',(req,res)=>{
        myDB.insertOne(req.body)
        .then(results=>{
            if(req.body.name == null || req.body.email == null)
            {
                logger.error("No User is added")
                res.send("Error")
            }
            else{
                logger.info(results)
                console.log(results)
                res.contentType('application/json')
                res.send(JSON.stringify(req.body))
            }          
        })
    }) 


    app.put('/updateUser',(req,res)=>{
        myDB.findOneAndUpdate(
            { id : req.body.id},
            {$set : {
                name : req.body.name,
                email: req.body.email
              }
            },
            {
                upsert: false
            }
        )
        .then(results=>{
            if(results.value==null)
            {
                logger.error("Error Occured")
                res.send("Error")
            }
            else{
                logger.info(results)
                console.log(results)
                res.contentType('application/json')
                res.send(JSON.stringify(req.body))
            }          
        })
    })
    
    
app.listen(port, ()=>{
    console.log('listening to port');
})

module.exports = app;

