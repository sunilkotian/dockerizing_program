const app = require('../app')
const supertest = require('supertest');


describe('CRUD OPERATION POST,GET,PUT', () => {

//Test cases for post method
    describe("post the data", ()=>{
        test('Expecting 200 status code',async()=>{
            const response=await supertest(app).post("/addUser").send({
             id :5,
             name : "sunil",
             email : "sunil@gmail.com",
             
            })
            expect(response.statusCode).toBe(200)
         })
        test('Expecting 404 status code',async()=>{
            const response=await supertest(app).get("/addUser").send({
                id :5,
                name : "sunil",
                email : 56,
            })
            expect(response.statusCode).toBe(404)
        })
        test('Expecting json format',async()=>{
            const response=await supertest(app).post("/addUser").send({
             id :5,
             name : "sunil",
             email : "sunil@gmail.com",
            })
            expect(response.headers['content-type']).toEqual(expect.stringContaining("json"))
         })
    })




//Test cases for get method
    describe("Get the data", ()=>{
        test('Expecting 200 status code',async()=>{
            const response=await supertest(app).get("/users")
            expect(response.statusCode).toBe(200)
         })
         test('Expecting 404 status code',async()=>{
            const response=await supertest(app).get("/user")
            expect(response.statusCode).toBe(404)
         })
         test('Expecting json format',async()=>{
            const response=await supertest(app).get("/users")
            expect(response.headers['content-type']).toEqual(expect.stringContaining("json"))
         })
    })



//Test cases for put method
    describe("Put the data", ()=>{
        test('Expecting 200 status code',async()=>{
            const response=await supertest(app).put("/updateUser").send({
                id: 5,
                name : "sunil",
                email : "sunil@gmail.com"
               })
            expect(response.statusCode).toBe(200)
         })


         test('Expecting 404 status code',async()=>{
          const response=await supertest(app).get("/update")
          expect(response.statusCode).toBe(404)
        }) 


         test('Expecting json format',async()=>{
            const response=await supertest(app).put("/updateUser").send({
                id: 5,
                name : "sunil",
                email : "sunil@gmail.com"
               })
            expect(response.headers['content-type']).toEqual(expect.stringContaining("json"))
         })      
    })

})